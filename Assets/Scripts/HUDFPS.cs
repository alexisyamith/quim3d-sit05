using System;
using UnityEngine;

public class HUDFPS : MonoBehaviour
{
    // Attach this to a GUIText to make a frames/second indicator.
    //
    // It calculates frames/second over each updateInterval,
    // so the display does not keep changing wildly.
    //
    // It is also fairly accurate at very low FPS counts (<10).
    // We do this not by simply counting frames per interval, but
    // by accumulating FPS for each frame. This way we end up with
    // correct overall FPS even if the interval renders something like
    // 5.5 frames.

    private float _accum; // FPS accumulated over the interval
    private int _frames; // Frames drawn over the interval
    private float _timeleft; // Left time for current interval
    public float updateInterval = 0.5F;

    public void Start()
    {
        /*hadron games
        if (!GetComponent<GUIText>())
        {
            Debug.Log("UtilityFramesPerSecond needs a GUIText component!");
            enabled = false;
            return;
        }
        _timeleft = updateInterval;
        //Application.targetFrameRate = 15;*/
    }

    public void Update()
    {
        /* hadron games
         _timeleft -= Time.deltaTime;
        _accum += Time.timeScale / Time.deltaTime;
        ++_frames;

        // Interval ended - update GUI text and start new interval
        if (_timeleft <= 0.0)
        {
            // display two fractional digits (f2 format)
            float fps = _accum / _frames;
            string format = String.Format("{0:F2} FPS", fps);
            GetComponent<GUIText>().text = format;

            //print(format);


            if (fps < 30)
                GetComponent<GUIText>().material.color = Color.yellow;
            else if (fps < 10)
                GetComponent<GUIText>().material.color = Color.red;
            else
                GetComponent<GUIText>().material.color = Color.green;
            //	DebugConsole.Log(format,level);
            _timeleft = updateInterval;
            _accum = 0.0F;
            _frames = 0;
        }*/
    }
}