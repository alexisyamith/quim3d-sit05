﻿using UnityEngine;

public class Tanque : MonoBehaviour
{
    /// <summary>
    /// Color inicial del contenido tanke
    /// </summary>
    private Color colorInicial;
    /// <summary>
    /// Cantidad de contenido actual
    /// </summary>
    public float contenidoActual;
    /// <summary>
    /// Cantidad maxima de contenido
    /// </summary>
    public float contenidoMaximoTanque;
    /// <summary>
    /// Cantidad minima de contenido
    /// </summary>
    private float contenidoMinimoTanque;
    /// <summary>
    /// Escala inicial de la geometria que representa el contenido del tanke
    /// </summary>
    private Vector3 escalaInicial;
    /// <summary>
    /// Escala de 0 a 1 del cilindro que llena el tanke
    /// </summary>
    private float escalaLocal;
    /// <summary>
    /// Escala correspondiente al scrip MeshMorpher 
    /// </summary>
    private float escalaMorpher;
    /// <summary>
    /// Limite maximo de la escala del tanke
    /// </summary>
    public float limiteEscalaTanque;
    /// <summary>
    /// Material del tanque
    /// </summary>
    public Material materialTanque;
    /// <summary>
    /// Clase para la modificacion del tanque de destilacion
    /// </summary>
    public MeshMorpher meshMorpherTanqueDestilacion;
    /// <summary>
    /// Clase que controla la pantalla que contiene entre sus opciones la calculadora
    /// </summary>
    private Pantalla pantalla;
    /// <summary>
    /// Clase que controla algunos datos del simulador
    /// </summary>
    public Simulador simulador;
    /// <summary>
    /// Transform para modificar la escala del tanke
    /// </summary>
    public Transform tanqueEscala;

    private void Start()
    {
        escalaMorpher = 0;
        escalaInicial = new Vector3(tanqueEscala.localScale.x, 0, tanqueEscala.localScale.z);

        if (name == "Tanque Fermentacion")
        {
            materialTanque = tanqueEscala.GetComponentInChildren<Renderer>().material;
            colorInicial = materialTanque.color;
        }

        pantalla = simulador.pantalla;
    }

    /// <summary>
    /// Agrega o disminuye contenido al tanke
    /// </summary>
    /// <param name="adicion"></param>
    /// <param name="restar"></param>
    public void AgregarContenido(float adicion, bool restar = false)
    {
        if (restar)//Restar contenido
        {
            if (contenidoActual >= contenidoMinimoTanque)
            {
                contenidoActual -= adicion;

                if (simulador.simuladorActivo.GetType() == typeof(SimuladorDestilacion))
                {
                    escalaMorpher -= adicion / 1.2f;
                    meshMorpherTanqueDestilacion.SetMorph(escalaMorpher);
                }

                if (simulador.simuladorActivo.GetType() == typeof(SimuladorFermentacion))
                {
                    adicion /= 100;
                    escalaLocal = tanqueEscala.localScale.y - adicion;
                    escalaLocal = Mathf.Clamp(escalaLocal, 0, limiteEscalaTanque);
                    tanqueEscala.localScale = new Vector3(tanqueEscala.localScale.x, escalaLocal,
                        tanqueEscala.localScale.z);
                }
            }
            else
            {//Vaciar totalmente el contenido del tanke
                if (simulador.simuladorActivo.GetType() == typeof(SimuladorFermentacion))
                    pantalla.MostrarMensajeFermentacion(simulador.simuladorActivo.msjTanqueVacio);
                simulador.simuladorActivo.tanqueVacio = true;
            }
        }
        else
        {
            if (contenidoActual <= contenidoMaximoTanque)//Agregar contenido al tanque
            {
                contenidoActual += adicion;

                if (simulador.simuladorActivo.GetType() == typeof(SimuladorDestilacion))
                {
                    escalaMorpher += adicion / 1.2f;
                    meshMorpherTanqueDestilacion.SetMorph(escalaMorpher);
                }

                if (simulador.simuladorActivo.GetType() == typeof(SimuladorFermentacion))
                {
                    adicion /= 100;
                    escalaLocal = tanqueEscala.localScale.y + adicion;
                    escalaLocal = Mathf.Clamp(escalaLocal, 0, limiteEscalaTanque);
                    tanqueEscala.localScale = new Vector3(tanqueEscala.localScale.x, escalaLocal, tanqueEscala.localScale.z);
                }
            }
            else
            {
                pantalla.MostrarMensajeFermentacion(simulador.simuladorActivo.msjTanqueLleno);
                simulador.simuladorActivo.tanqueDisponible = false;
            }

            simulador.simuladorActivo.tanqueVacio = false;
        }
    }

    /// <summary>
    /// Reinicia los valores y escala del tanque
    /// </summary>
    public void ReiniciarTanque()
    {
        contenidoActual = 0;
        escalaMorpher = 0;
        if (simulador.simuladorActivo.GetType() == typeof(SimuladorFermentacion))
        {
            materialTanque.color = colorInicial;
            tanqueEscala.localScale = escalaInicial;
        }
        if (simulador.simuladorActivo.GetType() == typeof(SimuladorDestilacion))
            meshMorpherTanqueDestilacion.SetMorph(0);
    }
}