﻿using UnityEngine;

namespace NSUtilities
{
    public class SyncGameObjectEnabled : MonoBehaviour
    {
        [SerializeField]
        private GameObject gameObjectObjective;

        private void OnEnable()
        {
            gameObjectObjective.SetActive(true);
        }

        private void OnDisable()
        {
            gameObjectObjective.SetActive(false);
        }

    } 
}
