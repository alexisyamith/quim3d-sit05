﻿using UnityEngine;

/// <summary>
/// Ventana en donde se muestra la informacion de la unidad
/// </summary>
public class PanelInformacionEjercicio : PanelInterfaz
{
    public GameObject btnIniciar;
    public GameObject btnCerrar;
    public GameObject btnAtras;
    public Pantalla pantalla;

    public void ClicBtnInfo()
    {
        btnIniciar.SetActive(false);
        btnCerrar.SetActive(true);
        btnAtras.SetActive(false);
        AbrirPanel();
        transform.parent.gameObject.SetActive(true);
    }

    public void BtnCerrarPanelInfo()
    {
        btnIniciar.SetActive(true);
        btnCerrar.SetActive(false);
        btnAtras.SetActive(true);
        CerrarPanel();
        pantalla.BtnCerrarInformacion();
    }
}
