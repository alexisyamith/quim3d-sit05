﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace NSCapturaPantalla
{
	public class CapturaPantalla : MonoBehaviour 
	{
        #region members

        #endregion

        #region members serialized

        #endregion

        #region properties

        #endregion

        #region methods Unity
        #endregion

        #region methods public
        /// <summary>
        /// Captura en una textura el rectTransform objetivo
        /// </summary>
        /// <param name="argRectTransformObjetivoCaptura">RectTransform que se quiere capturar</param>
        /// <returns>Textura del rectransform capturado</returns>
        public static Texture2D CapturarImagenRectTransform(RectTransform argRectTransformObjetivoCaptura)
        {
            var tmpEsquinasRectTransform = new Vector3[4];//obtengo los puntos de las esquinas del rectTransform que quiero capturar
            argRectTransformObjetivoCaptura.GetWorldCorners(tmpEsquinasRectTransform);
            var tmpAnchoTextura = Mathf.FloorToInt(tmpEsquinasRectTransform[2][0] - tmpEsquinasRectTransform[0][0]);
            var tmpAltoTextura = Mathf.FloorToInt(tmpEsquinasRectTransform[2][1] - tmpEsquinasRectTransform[0][1]);
            var texturaCaptura = new Texture2D(tmpAnchoTextura, tmpAltoTextura, TextureFormat.RGB24, false);
            var tmpRectCaptura = new Rect(Mathf.CeilToInt(tmpEsquinasRectTransform[0][0]), Mathf.CeilToInt(tmpEsquinasRectTransform[0][1]), tmpAnchoTextura, tmpAltoTextura);
            texturaCaptura.ReadPixels(tmpRectCaptura, 0, 0, false);
            texturaCaptura.Apply();

            return texturaCaptura;
        }


		#endregion

		#region methods private

		#endregion
	}
}
