﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase Principal
public class SimuladorDestilacion : SimuladorGenerico
{
    //Porcentajes penalidades segun proceso

    //Validaciones
    public delegate void Actualizar();

    public enum TipoValidacion
    {
        VALIDACION_PH,
        VALIDACIONTEMPERATURA,
        VALIDACIONSOLUCION
    };

    //Indicadores
    private float PH;
    public Ingrediente_Destilacion Solucion_Fermentada;
    public Ingrediente_Destilacion _base;
    public Actualizar _estado;
    public Ingrediente_Destilacion acido;
    private float alcoholEtilico;
    private float alcoholResultante;

    //Factores para adicionar insumos
    public GameObject btnAcido;
    public GameObject btnBase;
    public GameObject btnEncenderCondensador;
    public GameObject btnFijarTemperatura;
    public GameObject btnPantalla;
    public GameObject btnSolucionFermentada;
    public GameObject btnTemperaturaMas;
    public GameObject btnTemperaturaMenos;
    public GameObject btnVaciarTanque;
    private bool condensadorEncendido = false;
    private Vector3 condensadorLleno;
    private Vector3 condensadorVacio;
    public Transform contenidoCondensador;
    public float contenidoGas = 0;

    private float contenidoRecipiente;
    private List<DatosGrafico> datosGrafico;
    private float escalaActualRecipiente;
    private Vector3 escalaInicialRecipiente;
    private float factorAdiccionTemperatura = 3.0f;
    private float factorAgregarLiquido = 0.1f;
    private float factorAgregarLiquidoDestilado = 0.045f /*  0.02f*/;
    private float factorAgregarLiquidoPh = 0.01f;
    private float factorAgregarPH = 0.1f;
    private float factorPorcentajeDestilado = 70.37f;
    private float factorReducirPH = -0.1f;

    //Colores Temperatura
    private float frColorTemp = .00004f;
    private Material materialResistencia;

    //Mensajes
    private string msjCerrarValvulaEscape = "Antes de ingresar insumos desactive la válvula de alivio.";
    //private string msjCerrarValvulaEscape = "Before to add the solutions  deactivate  the relief valve.";

    private string msjSistemaMaxCapacidad1 = "Máxima capacidad combinada de líquidos y gases. Active la válvula de alivio si es necesario o termine el proceso.";
    //private string msjSistemaMaxCapacidad1 = "The tank is now full. Active exhaust relief valve.";
    public GameObject particulasGases;
    public ParticleSystem particulas;
    public ParticleSystem particulasChoque;

    public GameObject particulasLiquidoCondensado;
    private float penalizacion;
    private float porcentaje_Alcohol_Final = 0;
    private int porcentaje_PH_Muerte = 30;
    private int porcentaje_Solucion_Mala = 30;
    private int porcentaje_Temperatura_Mala = 30;
    public Transform refEscalaRecipiente;
    public Renderer resistencia;
    private float rpm;
    private AnimacionBotones scriptBtnCondAgua;
    private AnimacionBotones scriptBtnFijarTemp;

    //Referencias directas a los scripts de los botones
    private AnimacionBotones scriptBtnTempAgregar;
    private AnimacionBotones scriptBtnTempDisminuir;
    private AnimacionBotones scriptBtnValvulaAlivio;
    private float solucion_Maxima = 3.1f;
    private float solucion_Minima = 2.9f;
    public Tanque tanque;
    private float temperatura;
    private bool temperaturaAumentar = false;
    private bool temperaturaDisminuir = false;
    private bool temperaturaEstabilizada = true;
    private int temperatura_Maxima = 100;
    private int temperatura_Minima = 20;
    private float tiempoActual;
    private string tiempoSimulacion = "0"; //Verificar si se coloca en la pantalla
    public bool tiempoSimulacionCorrecta = false;
    private int tiempo_Disponible = 360;
    public TipoValidacion tipoValidacion;
    public bool vaciarTanque = false;
    private float valorMaximoPH = 7;
    private int valorMaximoTemperaturaBuena = 82;
    private float valorMinimoPH = 5;
    private int valorMinimoTemperaturaBuena = 78;
    public GameObject btnReset;

    // Use this for initialization
    private void Start()
    {
        msjCerrarValvulaEscape = controlIdiomas.Traductor(msjCerrarValvulaEscape);
        msjSistemaMaxCapacidad1 = controlIdiomas.Traductor(msjSistemaMaxCapacidad1);

        datosGrafico = new List<DatosGrafico>();
        //Funcionamiento EasyTouch
        refSeleccionMouse._DltSeleccion += On_TouchUpPantalla;
        refSeleccionMouse._DltSeleccion += On_TouchUpBtnFermentacion;

        //Estado Inicial
        CambiarEstado(EstadoSinActividad);

        //Asignacion Ref
        materialResistencia = resistencia.material;
        escalaInicialRecipiente = refEscalaRecipiente.localScale;
        condensadorVacio = contenidoCondensador.localScale;
        condensadorLleno = condensadorVacio + new Vector3(0, 0.038f, 0);
        scriptBtnTempAgregar = btnTemperaturaMas.GetComponent<AnimacionBotones>();
        scriptBtnTempDisminuir = btnTemperaturaMenos.GetComponent<AnimacionBotones>();
        scriptBtnFijarTemp = btnFijarTemperatura.GetComponent<AnimacionBotones>();
        scriptBtnCondAgua = btnEncenderCondensador.GetComponent<AnimacionBotones>();
        scriptBtnValvulaAlivio = btnVaciarTanque.GetComponent<AnimacionBotones>();
    }

    private void InicializarValores()
    {
        //Insumos
        Solucion_Fermentada.valor = 0;
        simulador.pantalla.solucionDestilacionTexto.text = "0.00";
        simulador.pantalla.pantalla_Solucion_Destilacion.text = "0.00";
        Solucion_Fermentada.scriptBtn.Click(false);
        Solucion_Fermentada.valvulaAbierta = false;

        acido.valor = 0;
        simulador.pantalla.acidoDestilacionTexto.text = "0.00";
        simulador.pantalla.pantalla_Acido_Destilacion.text = "0.00";
        acido.scriptBtn.Click(false);
        acido.valvulaAbierta = false;

        _base.valor = 0;
        simulador.pantalla.baserDestilacionTexto.text = "0.00";
        simulador.pantalla.pantalla_Base_Destilacoin.text = "0.00";
        _base.scriptBtn.Click(false);
        _base.valvulaAbierta = false;

        //Temperatura
        temperaturaEstabilizada = true;
        materialResistencia.color = new Color(1, 0.8f, 0.8f, 1);
        temperatura = 23.0f;
        simulador.pantalla.temperaturaDestilacionTexto.text = temperatura.ToString();
        simulador.pantalla.pantalla_Temperatura_Destilacion.text = temperatura.ToString();
        temperaturaAumentar = false;
        temperaturaDisminuir = false;
        temperaturaEstabilizada = true;
        scriptBtnTempAgregar.Click(false);
        scriptBtnTempDisminuir.Click(false);
        scriptBtnFijarTemp.Click(true);

        //Condensador Agua
        condensadorEncendido = false;
        simulador.pantalla.indicadores_Nivel_Agua_Destilacion_Texto.text = controlIdiomas.Traductor("Apagado");
        scriptBtnCondAgua.Click(false);
        contenidoCondensador.localScale = condensadorVacio;

        //PH
        PH = 4;
        simulador.pantalla.indicadores_PH_Destilacion.text = "4";

        //Tiempos 
        simulador.pantalla.indicador_Tiempo_Usuario_Destilacion.text = "00:00:00";
        tiempoActual = 0;

        //Tanques
        tanqueDisponible = true;
        vaciarTanque = false;
        tanque.ReiniciarTanque();

        //Recipiente
        refEscalaRecipiente.localScale = escalaInicialRecipiente;
        contenidoRecipiente = 0;

        //Contenido Gas
        contenidoGas = 0;
        particulasGases.SetActive(false);
        particulasLiquidoCondensado.SetActive(false);

        porcentaje_Alcohol_Final = 0;

        //Alcohol Destilado 		
        simulador.pantalla.indicadores_Alcohol_Destilacion.text = "0";

        scriptBtnValvulaAlivio.Click(false);

        //tanqueVacio = false;  //POR QUE ESTA ARRANCANDO EN FALSE

        ReiniciarGrafica();
    }

    void ReiniciarGrafica()
    {
        datosGrafico = new List<DatosGrafico>();
        // Aqui se añaden dos a proposito. El segundo es donde termina el segmento de temperatura
        datosGrafico.Add(new DatosGrafico() { tiempo = 0, temperatura = 0, porcentajeAlcohol = 0 });
        datosGrafico.Add(new DatosGrafico() { tiempo = 0, temperatura = 0, porcentajeAlcohol = 0 });
    }

    // Update is called once per frame
    private void Update()
    {
        _estado();
    }

    //ESTADOS
    //Cambiar Estado
    private void CambiarEstado(Actualizar estado)
    {
        if (estado == EstadoSimulacion)
        {
            Simulador.activeBtnReset = true;
            ejecutoVariosEjercicios = true;
        }
        _estado = estado;
    }

    //Estado Inicial
    private void EstadoIniciando()
    {
        ContarTiempoSession();
    }

    //Estado Sin Actividad
    private void EstadoSinActividad()
    {

    }

    //Estado Simulacion
    private void EstadoSimulacion()
    {
        //Cuenta tiempo de la fermentacion
        ContarTiempo();
        //Cuenta tiempo de la Session
        ContarTiempoSession();

        ActualizarDatos();

        //Vaciado del  tanque
        if (vaciarTanque)
        {
            if (!tanqueVacio)
            {
                tanqueDisponible = false;

                if (Solucion_Fermentada.valor > 0)
                {
                    Solucion_Fermentada.valor -= factorAgregarLiquido * Time.deltaTime;
                    tanque.AgregarContenido(factorAgregarLiquido * Time.deltaTime, true);
                    simulador.pantalla.solucionDestilacionTexto.text = Solucion_Fermentada.valor.ToString("f2");
                }
                else
                {
                    Solucion_Fermentada.valor = 0;
                    simulador.pantalla.solucionDestilacionTexto.text = "0.00";
                }

                if (acido.valor > 0)
                {
                    acido.valor -= factorAgregarLiquidoPh * Time.deltaTime;
                    tanque.AgregarContenido(factorAgregarLiquidoPh * Time.deltaTime, true);
                    simulador.pantalla.acidoDestilacionTexto.text = acido.valor.ToString("f2");
                    FijarPH(factorAgregarPH);
                }
                else
                {
                    acido.valor = 0;
                    simulador.pantalla.acidoDestilacionTexto.text = "0.00";
                }

                if (_base.valor > 0)
                {
                    _base.valor -= factorAgregarLiquidoPh * Time.deltaTime;
                    tanque.AgregarContenido(factorAgregarLiquidoPh * Time.deltaTime, true);
                    simulador.pantalla.baserDestilacionTexto.text = _base.valor.ToString("f2");
                    FijarPH(factorReducirPH);
                }
                else
                {
                    _base.valor = 0;
                    simulador.pantalla.baserDestilacionTexto.text = "0.00";
                }
            }
            else
            {
                vaciarTanque = false;
                tanqueDisponible = true;
            }
        }

        //Accionamiento de valvulas
        if (tanqueDisponible)
        {
            //Verifica la maxima capacidad del sistema (tanque principal, recipiente y gas) y que
            if (tanque.contenidoActual + _base.valor + acido.valor + contenidoRecipiente + contenidoGas >= 5)
            {
                //if(tanque.contenidoActual <= 0){
                tanqueDisponible = false;
                simulador.pantalla.MostrarMensajeFermentacion(msjSistemaMaxCapacidad1, 1);
                return;
                //}
            }

            //Cuando se abre la valvula del azucar
            if (Solucion_Fermentada.valvulaAbierta)
            {
                Solucion_Fermentada.valor += factorAgregarLiquido * Time.deltaTime;
                tanque.AgregarContenido(factorAgregarLiquido * Time.deltaTime);
                simulador.pantalla.solucionDestilacionTexto.text = Solucion_Fermentada.valor.ToString("f2");
            }
            //Cuando se abre la valvula del Acido
            if (acido.valvulaAbierta)
            {
                acido.valor += factorAgregarLiquidoPh * Time.deltaTime;
                tanque.AgregarContenido(factorAgregarLiquidoPh * Time.deltaTime);
                simulador.pantalla.acidoDestilacionTexto.text = acido.valor.ToString("f2");
                FijarPH(factorReducirPH);
            }
            //Cuando se abre la valvula de la Base
            if (_base.valvulaAbierta)
            {
                _base.valor += factorAgregarLiquidoPh * Time.deltaTime;
                tanque.AgregarContenido(factorAgregarLiquidoPh * Time.deltaTime);
                simulador.pantalla.baserDestilacionTexto.text = _base.valor.ToString("f2");
                FijarPH(factorAgregarPH);
            }
        }
        //Cerrar valvulas si esta lleno el tanque
        else
        {
            Solucion_Fermentada.valvulaAbierta = false;
            acido.valvulaAbierta = false;
            _base.valvulaAbierta = false;

            //TODO llamar esto solo una vez
            Solucion_Fermentada.scriptBtn.Click(false);
            _base.scriptBtn.Click(false);
            acido.scriptBtn.Click(false);
        }

        //Adiccionar y restar temperatura atomaticamente
        if (!temperaturaEstabilizada)
        {
            if (temperaturaAumentar && temperatura <= temperatura_Maxima)
            {
                temperatura += factorAdiccionTemperatura * Time.deltaTime;
                resistencia.gameObject.GetComponent<Renderer>().material.color = new Color(1, materialResistencia.color.g - frColorTemp,
                    materialResistencia.color.b - frColorTemp, 1);
                //Debug.LogError(resistencia.gameObject.renderer.material.color);
            }
            if (temperaturaDisminuir && temperatura >= temperatura_Minima)
            {
                temperatura -= factorAdiccionTemperatura * Time.deltaTime;
                resistencia.gameObject.GetComponent<Renderer>().material.color = new Color(1, materialResistencia.color.g + frColorTemp,
                    materialResistencia.color.b + frColorTemp, 1);
                //Debug.LogError(resistencia.gameObject.renderer.material.color);
            }
            simulador.pantalla.temperaturaDestilacionTexto.text = temperatura.ToString("f1");
        }

        //Proceso para convertir gas a liquido
        if (condensadorEncendido)
        {
            if (contenidoGas > 0)
            {
                contenidoGas -= factorAgregarLiquido / 2 * Time.deltaTime;
                AgregarContenidoRecipiente(factorAgregarLiquidoDestilado / 2 * Time.deltaTime);
            }
        }
        if (temperatura >= 79)
        {
            if (tanque.contenidoActual > 0)
            {
                contenidoGas += factorAgregarLiquido / 2 * Time.deltaTime;
                tanque.AgregarContenido(factorAgregarLiquido / 2 * Time.deltaTime, true);
            }
        }

        // si es el primer dato 
        if (datosGrafico.Count == 0)
        {
            ReiniciarGrafica();
        }

        float roundTemp = ((int)(temperatura * 10)) / 10.0f;
        float roundAlcohol = ((int)(porcentaje_Alcohol_Final * 10)) / 10.0f;

        if (!Mathf.Approximately(datosGrafico[datosGrafico.Count - 1].temperatura, roundTemp) || !Mathf.Approximately(datosGrafico[datosGrafico.Count - 1].porcentajeAlcohol, roundAlcohol))
        {
            // Aqui se añaden dos a proposito. El segundo es donde termina el segmento de temperatura
            datosGrafico.Add(new DatosGrafico
            {
                tiempo = tiempoActual,
                temperatura = roundTemp,
                porcentajeAlcohol = roundAlcohol
            });

            datosGrafico.Add(new DatosGrafico
            {
                tiempo = tiempoActual,
                temperatura = roundTemp,
                porcentajeAlcohol = roundAlcohol
            });
        }
        else
        {
            DatosGrafico dato = datosGrafico[datosGrafico.Count - 1];
            dato.tiempo = tiempoActual;
            datosGrafico[datosGrafico.Count - 1] = dato;
        }
    }

    //Estado SimulacionTerminada
    private void EstadoSimulacionTerminada()
    {

    }

    //Agregar contenido al recipiente
    public void AgregarContenidoRecipiente(float adicion)
    {
        contenidoRecipiente += adicion;
        adicion /= 100;
        escalaActualRecipiente = Mathf.Clamp(refEscalaRecipiente.localScale.y + adicion, 0, 0.05f);
        refEscalaRecipiente.localScale = new Vector3(refEscalaRecipiente.localScale.x, escalaActualRecipiente,
            refEscalaRecipiente.localScale.z);
    }

    //ContarTiempo
    private void ContarTiempo()
    {
        tiempoActual += Time.deltaTime;
        TimeSpan t = TimeSpan.FromSeconds(tiempoActual);
        //mostrar en el hud
        tiempoSimulacion = string.Format("{0:D2}:{1:D2}:{2:D2}", t.Minutes, t.Seconds, t.Milliseconds / 10);
        simulador.pantalla.indicador_Tiempo_Usuario_Destilacion.text = tiempoSimulacion;

        //Verfica que haya pasado el tiempo de simulacion y actualiza datos de adiccion de tiempo
        if (tiempoActual >= tiempo_Disponible)
        {
            CambiarEstado(EstadoSimulacionTerminada);
            VerificarResultadoEjercicio();
        }
    }

    //Fijar PH
    private void FijarPH(float _factorPH)
    {
        PH += _factorPH * Time.deltaTime;
        simulador.pantalla.indicadores_PH_Destilacion.text = Mathf.Clamp(PH, 0, 12).ToString("f2");
    }

    // se llama desde el hud cuando presiona iniciar
    public override void BtnIniciar(bool _practicaLibre)
    {
        practicaLibre = _practicaLibre;
        Simulador.ejecutandoEjercicio = false;
        CambiarEstado(EstadoIniciando);
        InicializarValores();
        usuarioEntroEstacion = true;
        if (!_practicaLibre)
        {
            ReiniciarGrafica();
        }
    }

    private void ClickBtnSolucion()
    {
        if (_estado == EstadoIniciando)
            CambiarEstado(EstadoSimulacion);
        Solucion_Fermentada.valvulaAbierta = !Solucion_Fermentada.valvulaAbierta;
        Solucion_Fermentada.scriptBtn.Click(Solucion_Fermentada.valvulaAbierta);
        if (vaciarTanque)
            simulador.pantalla.MostrarMensajeFermentacion(msjCerrarValvulaEscape);
    }

    private void ClickBtnAcido()
    {
        if (_estado == EstadoIniciando)
            CambiarEstado(EstadoSimulacion);
        acido.valvulaAbierta = !acido.valvulaAbierta;
        acido.scriptBtn.Click(acido.valvulaAbierta);
        if (vaciarTanque)
            simulador.pantalla.MostrarMensajeFermentacion(msjCerrarValvulaEscape);
    }

    private void ClickBtnBase()
    {
        if (_estado == EstadoIniciando)
            CambiarEstado(EstadoSimulacion);
        _base.valvulaAbierta = !_base.valvulaAbierta;
        _base.scriptBtn.Click(_base.valvulaAbierta);
        if (vaciarTanque)
            simulador.pantalla.MostrarMensajeFermentacion(msjCerrarValvulaEscape);
    }

    private void ClickBtnTempMas()
    {
        if (!temperaturaAumentar)
        {
            temperaturaAumentar = true;
            temperaturaDisminuir = false;
            temperaturaEstabilizada = false;
            //Configurar Botones
            scriptBtnTempAgregar.Click(true);
            scriptBtnTempDisminuir.Click(false);
            scriptBtnFijarTemp.Click(false);
        }
    }

    private void ClickBtnTempMenos()
    {
        if (!temperaturaDisminuir)
        {
            temperaturaAumentar = false;
            temperaturaDisminuir = true;
            temperaturaEstabilizada = false;
            //Configurar Botones
            scriptBtnTempDisminuir.Click(true);
            scriptBtnTempAgregar.Click(false);
            scriptBtnFijarTemp.Click(false);
        }
    }

    private void ClickBtnFijarTemp()
    {
        if (!temperaturaEstabilizada)
        {
            temperaturaEstabilizada = true;
            temperaturaAumentar = false;
            temperaturaDisminuir = false;
            //Configurar Botones
            scriptBtnFijarTemp.Click(true);
            scriptBtnTempDisminuir.Click(false);
            scriptBtnTempAgregar.Click(false);
        }
    }

    private void ClickBtnVaciarTanque()
    {
        if (_estado == EstadoIniciando)
            CambiarEstado(EstadoSimulacion);
        vaciarTanque = !vaciarTanque;
        tanqueDisponible = !tanqueDisponible;
        scriptBtnValvulaAlivio.Click(vaciarTanque);
    }

    private void ClickBtnEncendidoCondensador()
    {
        if (_estado == EstadoIniciando)
            CambiarEstado(EstadoSimulacion);
        if (!condensadorEncendido)
        {
            condensadorEncendido = true;
            simulador.pantalla.indicadores_Nivel_Agua_Destilacion_Texto.text = controlIdiomas.Traductor("Encendido");
            scriptBtnCondAgua.Click(condensadorEncendido);
            iTween.ScaleTo(contenidoCondensador.gameObject, condensadorLleno, 8);
        }
    }

    public override void ActualizarDatosReporte()
    {
        CambiarEstado(EstadoSinActividad);
        tablaDatos.Clear();

        tablaDatos.Add(Solucion_Fermentada.valor.ToString("f2") + " L");
        tablaDatos.Add(acido.valor.ToString("f2") + " L");
        tablaDatos.Add(_base.valor.ToString("f2") + " L");
        tablaDatos.Add(temperatura.ToString("f1"));
        tablaDatos.Add(contenidoRecipiente.ToString("f2") + " L");
        tablaDatos.Add(porcentaje_Alcohol_Final.ToString("f2") + " %");
        tablaDatos.Add(PH.ToString("f2"));

        simulador.pantalla.reporte_Fecha.text = System.DateTime.Now.ToString("dd-MM-yyyy");
        tablaDatos.Add(simulador.pantalla.indicadores_Nivel_Agua_Destilacion_Texto.text);
        simulador.simuladorActivo.reporte.GuardarTablaDatos(tablaDatosTextos, tablaDatos);
        simulador.pantalla.textoGrafica.text = nombreGrafica;
        simulador.simuladorActivo.reporte.GuardarNombrePDF(nombrePDF);
    }

    //Calcula Resultado del alcohol destilado
    public float ValidarDatos(TipoValidacion tipo)
    {
        if (tipo == TipoValidacion.VALIDACION_PH)
        {
            if (ValorRango(PH, valorMinimoPH, valorMaximoPH))
                penalizacion = 0;
            else
                penalizacion = porcentaje_PH_Muerte;
        }
        if (tipo == TipoValidacion.VALIDACIONTEMPERATURA)
        {
            if (ValorRango(temperatura, valorMinimoTemperaturaBuena, valorMaximoTemperaturaBuena))
                penalizacion = 0;
            else
                penalizacion = porcentaje_Temperatura_Mala;
        }
        if (tipo == TipoValidacion.VALIDACIONSOLUCION)
        {
            if (ValorRango(Solucion_Fermentada.valor, solucion_Minima, solucion_Maxima))
                penalizacion = 0;
            else
                penalizacion = porcentaje_Solucion_Mala;
        }
        return penalizacion;
    }

    //Actualiza Datos cada 3 segundos
    float deltaParticulas = 0;

    public void ActualizarDatos()
    {
        //  yield return new WaitForSeconds(0.1f);
        if (_estado == EstadoSimulacion)
        {
            //particulasGases.SetActive(contenidoGas > 0 ? true : false);
            if (contenidoGas > 0)
            {
                particulasGases.SetActive(true);
                particulas.maxParticles = 100;
                particulasChoque.maxParticles = 100;
                deltaParticulas = 0;
            }
            else
            {

                deltaParticulas += Time.deltaTime * 20;
                int valor = (int)(100 - deltaParticulas);
                particulas.maxParticles = Mathf.Clamp(valor, 0, 100);
                particulasChoque.maxParticles = Mathf.Clamp(valor, 0, 100);

            }

            if (condensadorEncendido)
                particulasLiquidoCondensado.SetActive(contenidoGas > 0 ? true : false);

            //Verifica que ya este en estado liquido para calcular el alcohol destilado
            porcentaje_Alcohol_Final = 0;
            if (contenidoRecipiente > 0)
            {
                porcentaje_Alcohol_Final = contenidoRecipiente * factorPorcentajeDestilado;
                porcentaje_Alcohol_Final -= ValidarDatos(TipoValidacion.VALIDACION_PH);
                porcentaje_Alcohol_Final -= ValidarDatos(TipoValidacion.VALIDACIONTEMPERATURA);
                porcentaje_Alcohol_Final -= ValidarDatos(TipoValidacion.VALIDACIONSOLUCION);
            }
            porcentaje_Alcohol_Final = Mathf.Clamp(porcentaje_Alcohol_Final, 0, 95);
            simulador.pantalla.indicadores_Alcohol_Destilacion.text = porcentaje_Alcohol_Final.ToString("f0");
        }
    }

    //Calcula valor entre un rango
    public bool ValorRango(float valor, float minimo, float maximo)
    {
        return valor >= minimo && valor <= maximo ? true : false;
    }

    //Verifica que este bien ejecutado el ejercicio
    private void VerificarResultadoEjercicio()
    {

#if MODO_RAPIDO
		porcentaje_Alcohol_Final = 95;
#endif

        if (porcentaje_Alcohol_Final == 95)
            simulador.pantalla.MostrarResultadoEjercicio(true);
        else
            simulador.pantalla.MostrarResultadoEjercicio(false);
        simulador.pantalla.panelEntradaDatos.gameObject.SetActive(false);
    }

    //Cuando se sale del simulador de destilacion se llama esta funcion publica
    public void SalidaDestilacion()
    {
        CambiarEstado(EstadoSinActividad);
        InicializarValores();
    }

    //Obtener datos para graficar
    public override float[] ObtenerDatosGrafica()
    {
        var _datos = new float[datosGrafico.Count * 3];
        int i = 0;
        int numDatos = datosGrafico.Count;
        foreach (DatosGrafico datoGrafico in datosGrafico)
        {
            _datos[i] = datoGrafico.tiempo;
            _datos[numDatos + i] = datoGrafico.temperatura;
            _datos[numDatos * 2 + i] = datoGrafico.porcentajeAlcohol;
            i++;
        }
        return _datos;
    }

    //Preguntas desde archivo externo
    public void RecibirPreguntasSimulador(string preguntas)
    {
        externalQuestions = preguntas.Split('&');
        refExternalQuestions.Add(externalQuestions[0]);
        refExternalQuestions.Add(externalQuestions[1]);
        refExternalQuestions.Add(externalQuestions[2]);
        refExternalQuestions.Add(externalQuestions[3]);
        refExternalQuestions.Add(externalQuestions[4]);
    }

    #region Region_EasyTouch

    private void On_TouchUpPantalla(GameObject argBotonseleccionado)
    {
        if (simulador.simuladorActivo.gameObject == gameObject && usuarioEntroEstacion)
        {
            if (argBotonseleccionado == btnPantalla.gameObject)
            {
                if (!Pantalla.pantallaActiva && Simulador.simulacionIniciada)
                {
                    argBotonseleccionado.GetComponent<AnimacionBotones>().Click();
                    simulador.pantalla.AccionarPantalla();
                    simulador.pantalla.ActualizarDatosPantalla();
                }
            }
        }
    }

    public override void MostrarPantalla()
    {
        if (!Pantalla.pantallaActiva && Simulador.simulacionIniciada)
        {
            simulador.pantalla.AccionarPantalla();
            simulador.pantalla.ActualizarDatosPantalla();
            return;
        }
        else
            print("cierre la pantalla o inicie el simulador");
    }

    void On_TouchUpReset(GameObject argBotonseleccionado)
    {
        if (simulador.simuladorActivo.gameObject == gameObject && usuarioEntroEstacion)
        {
            if (argBotonseleccionado == btnReset)
            {
                if (!Pantalla.pantallaActiva && Simulador.simulacionIniciada && Simulador.activeBtnReset)
                {
                    print("Simulador.activeBtnReset: " + Simulador.activeBtnReset);
                    argBotonseleccionado.GetComponent<AnimacionBotones>().Click();
                    simulador.pantalla.BtnReintentar();
                    ReiniciarGrafica();
                }
            }
        }
    }

    private void On_TouchUpBtnFermentacion(GameObject argBotonseleccionado)
    {
        if (simulador.simuladorActivo.gameObject == gameObject)
        {
            //Valvuala Solucion
            if (argBotonseleccionado == btnSolucionFermentada)
            {
                ClickBtnSolucion();
                simulador.pantalla.DibujarGrafico(false);
            }
            //Valvuala Acido
            if (argBotonseleccionado == btnAcido)
            {
                ClickBtnAcido();
                simulador.pantalla.DibujarGrafico(false);
            }
            //Valvuala Base
            if (argBotonseleccionado == btnBase)
            {
                ClickBtnBase();
                simulador.pantalla.DibujarGrafico(false);
            }
            //Valvuala Temperatura Mas
            if (argBotonseleccionado == btnTemperaturaMas)
            {
                ClickBtnTempMas();
                simulador.pantalla.DibujarGrafico(false);
            }
            //Valvuala Temperatura Menos
            if (argBotonseleccionado == btnTemperaturaMenos)
            {
                ClickBtnTempMenos();
                simulador.pantalla.DibujarGrafico(false);
            }
            //Boton Fijar Temperatura 
            if (argBotonseleccionado == btnFijarTemperatura)
            {
                ClickBtnFijarTemp();
                simulador.pantalla.DibujarGrafico(false);
            }
            //Btn Vaciar Tanque
            if (argBotonseleccionado == btnVaciarTanque)
            {
                ClickBtnVaciarTanque();
                simulador.pantalla.DibujarGrafico(false);
            }
            //Btn Condensador
            if (argBotonseleccionado == btnEncenderCondensador)
            {
                ClickBtnEncendidoCondensador();
                simulador.pantalla.DibujarGrafico(false);
            }
        }
    }

    private void OnDisable()
    {
        UnsubscribeEvent();
    }

    private void OnDestroy()
    {
        UnsubscribeEvent();
    }

    private void UnsubscribeEvent()
    {
        refSeleccionMouse._DltSeleccion -= On_TouchUpPantalla;
        refSeleccionMouse._DltSeleccion -= On_TouchUpReset;
        refSeleccionMouse._DltSeleccion -= On_TouchUpBtnFermentacion;
    }

    #endregion

    public struct DatosGrafico
    {
        public float porcentajeAlcohol;
        public float temperatura;
        public float tiempo;
    };
}

//Clase Ingredientes
[Serializable]
public class Ingrediente_Destilacion
{
    public enum Tipo
    {
        SOLUCION_FERMENTADA,
        BASE,
        ACIDO
    };

    public enum _Color
    {
        Blanco,
        Transparente,
        Verde
    };

    public _Color color;
    public string nombre;
    public AnimacionBotones scriptBtn;
    public SimuladorDestilacion simuladorDestilacion;
    public Tipo tipo;
    public float valor;
    public bool valvulaAbierta;
}