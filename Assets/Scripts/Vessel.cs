﻿using System.Collections;
using UnityEngine;

public class Vessel : MonoBehaviour
{
    private float liquidTime;
    public float maxVolume;
    private MeshMorpher meshMorpher;
    private float morphActual;
    private float morphValue;
    public Renderer piloto;
    private AnimacionBotones refBtnScript;
    private float scaleLiquid;
    public Pantalla screen;
    public float solA;
    public float solB;
    public float solC;
    public float solD;
    public SimuladorAminoAcido.SolutionType solutionType;
    public float solvent;
    public float totalLiquid;
    public bool useMorpher = false;
    public GameObject vesselLiquid;
    private Vector3 vesselLiquidInitial;
    public GameObject vesselModel;

    public void Empty()
    {
        solA = 0;
        solB = 0;
        solC = 0;
        solD = 0;
        solvent = 0;
    }

    // Add all from other vessel
    public void SendAllLiquid(Vessel v)
    {
        v.solA += solA;
        v.solB += solB;
        v.solC += solC;
        v.solD += solD;
        v.solvent += solvent;
        Empty();

        // Animar el vessel al que se le esta enviando el liquido 
        v.InitFillLiquidAnimation();

        scaleLiquid = vesselLiquid.transform.localScale.y;
        morphActual = morphValue;
        StartCoroutine(RemoveLiquid());
    }

    public void InitFillLiquidAnimation()
    {
        morphActual = morphValue;
        scaleLiquid = vesselLiquid.transform.localScale.y;
        StopCoroutine("AddLiquidAnimation");
        StartCoroutine("AddLiquidAnimation");
    }

    // Pasa cuando un frasco de solucion envia a un frasco de proceso.
    public void ReciveSolution(int value, SimuladorAminoAcido.SolutionType type, Material _material)
    {
        if (type != SimuladorAminoAcido.SolutionType.solvent)
            vesselModel.GetComponent<Renderer>().material = _material;

        if (type == SimuladorAminoAcido.SolutionType.solA)
        {
            solA += value;
            totalLiquid = solA;
            screen.input_solutionA.text = solA.ToString("f2");
        }
        if (type == SimuladorAminoAcido.SolutionType.solB)
        {
            solB += value;
            totalLiquid = solB;
            screen.input_solutionB.text = solB.ToString("f2");
        }
        if (type == SimuladorAminoAcido.SolutionType.solC)
        {
            solC += value;
            totalLiquid = solC;
            screen.input_solutionC.text = solC.ToString("f2");
        }
        if (type == SimuladorAminoAcido.SolutionType.solD)
        {
            solD += value;
            totalLiquid = solD;
            screen.input_solutionD.text = solD.ToString("f2");
        }
        if (type == SimuladorAminoAcido.SolutionType.solvent)
        {
            solvent += value;
            totalLiquid = solvent;
            screen.input_solvent.text = solvent.ToString("f2");
        }

        morphActual = morphValue;
        scaleLiquid = vesselLiquid.transform.localScale.y;
        StopCoroutine("AddLiquidAnimation");
        StartCoroutine("AddLiquidAnimation");
    }

    // Pasa solo cuando un frasco de solucion y envia a un frasco de proceso
    public void SendSolution(Vessel vessel, int value)
    {
        if (piloto)
        {
            refBtnScript.StopCoroutine(refBtnScript.ActivarBtInstantaneamente());
            refBtnScript.StartCoroutine(refBtnScript.ActivarBtInstantaneamente());
        }

        //TODO : animar que se rebajo la cantidad de solucion en ese frasco
        vessel.ReciveSolution(value, solutionType, vesselModel.GetComponent<Renderer>().material);
    }

    private void Awake()
    {
        //Reference initial content , Btn Script
        if (useMorpher)
            meshMorpher = vesselLiquid.GetComponent<MeshMorpher>();
        else
            vesselLiquidInitial = vesselLiquid.transform.localScale;
        if (piloto)
            refBtnScript = piloto.GetComponent<AnimacionBotones>();
    }

    //  frascos de acondicionamiento les cabe 100
    //  frasco final de prueba 300 ml 
    //  solucion 1 litro y arranca lleno
    private IEnumerator AddLiquidAnimation()
    {
        yield return new WaitForSeconds(0);
        liquidTime += Time.deltaTime / 3;
        totalLiquid = solA + solB + solC + solD + solvent;

        if (useMorpher)
        {
            // va de 0 a  meshMorpher.m_Meshes.Length -  1;
            // liquido va de 0 a 100 
            int meshes = meshMorpher.m_Meshes.Length - 1;
            float morpherTime = (totalLiquid / maxVolume) * meshes;
            morphValue = Mathf.Lerp(morphActual, morpherTime, liquidTime);
            meshMorpher.SetMorph(morphValue);
            screen.indicator_aminoAcidProcess.text = totalLiquid.ToString();
        }
        else
        {
            float nuevoY = Mathf.Lerp(scaleLiquid, totalLiquid / maxVolume, liquidTime);
            vesselLiquid.transform.localScale = new Vector3(vesselLiquid.transform.localScale.x, nuevoY,
                vesselLiquid.transform.localScale.z);

            //No update indicator condionnigMP when add liquid to finalVessel
            if (solutionType != SimuladorAminoAcido.SolutionType.other)
                screen.indicator_conditioningMP.text = totalLiquid.ToString();
        }


        if (liquidTime < 1)
            StartCoroutine("AddLiquidAnimation");
        else
        {
            liquidTime = 0;
            SimuladorAminoAcido.sendingContent = false;
        }
    }

    private IEnumerator RemoveLiquid()
    {
        yield return new WaitForSeconds(0);

        liquidTime += Time.deltaTime / 3;

        if (useMorpher)
        {
            // va de 0 a  meshMorpher.m_Meshes.Length -  1;
            // liquido va de 0 a 100 
            morphValue = Mathf.Lerp(morphActual, 0, liquidTime);
            meshMorpher.SetMorph(morphValue);           
            screen.indicator_aminoAcidProcess.text = "0";
        }
        else
        {
            float nuevoY = Mathf.Lerp(scaleLiquid, 0, liquidTime);
            vesselLiquid.transform.localScale = new Vector3(vesselLiquid.transform.localScale.x, nuevoY,
                vesselLiquid.transform.localScale.z);
            screen.indicator_conditioningMP.text = "0";
        }

        if (liquidTime < 1)
            StartCoroutine(RemoveLiquid());
        else
        {
            liquidTime = 0;
            totalLiquid = 0;
        }
    }

    //Initilize scale when restart
    public void InitializeSize()
    {
        vesselLiquid.transform.localScale = vesselLiquidInitial;
    }

    //Restart Morpher
    public void Restart()
    {
        meshMorpher.SetMorph(0);
        morphValue = 0;
        morphActual = 0;
    }
}